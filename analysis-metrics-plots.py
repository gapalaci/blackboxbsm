# %%
import os

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from utils.analysis import *

# %%
# Preamble
sns.set_theme("paper")
sns.set(font_scale=font_scale)
run_name = "run_fixed"
path_plots = os.path.join(run_name, "plots")
os.makedirs(path_plots, exist_ok=True)

# %%
full_episode_metrics = pd.read_csv(os.path.join(run_name, "full_episode_metrics.csv"), index_col=0)
grouped_episode_metrics = pd.read_csv(os.path.join(run_name, "grouped_episode_metrics.csv"))
trial_evolution = pd.read_csv(os.path.join(run_name, "trial_evolution.csv"), index_col=0)
# %%
full_episode_metrics["Sampler"] = pd.Categorical(
    full_episode_metrics["Sampler"].tolist(), categories=samplers
)
grouped_episode_metrics["Sampler"] = pd.Categorical(
    grouped_episode_metrics["Sampler"].tolist(), categories=samplers
)
trial_evolution["Sampler"] = pd.Categorical(
    trial_evolution["Sampler"].tolist(), categories=samplers
)

# %%
fig, axs = plt.subplots(2, 2, figsize=figsize_square_grid)
for idx, model in enumerate(["cMSSM", "pMSSM"]):
    for jdx, micromegas in enumerate([False, True]):
        axs[idx, jdx].set_title(
            r"{}, {}".format(
                model, r"$C(m_{h^0} \cap \Omega_{DM} h^2)$" if micromegas else r"$C(m_{h^0})$"
            )
        )
        sns.lineplot(
            data=trial_evolution.query(
                f"Model == '{model}' & MicrOMEGAS == {micromegas}  "
            ).replace(nice_sampler_names),
            hue="Sampler",
            x="step",
            y="value",
            legend=True,
            ax=axs[idx, jdx],
        )
        axs[idx, jdx].set_yscale("log")
        axs[idx, jdx].set_xlabel("")
        axs[idx, jdx].set_ylabel("")
        if idx == 1:
            axs[idx, jdx].set_xlabel("Trial Number")
        if jdx == 0:
            axs[idx, jdx].set_ylabel("50 Trials Rolling \n Average Loss")

        if idx == 0 and jdx == 1:
            axs[idx, jdx].legend(loc="best")
        else:
            axs[idx, jdx].get_legend().remove()

plt.savefig(os.path.join(path_plots, "rolling_average_value_evolution.pdf"), bbox_inches="tight")

# %%
fig, axs = plt.subplots(2, 2, figsize=figsize_square_grid)
for idx, model in enumerate(["cMSSM", "pMSSM"]):
    for jdx, micromegas in enumerate([False, True]):
        axs[idx, jdx].set_title(
            r"{}, {}".format(
                model, r"$C(m_{h^0} \cap \Omega_{DM} h^2)$" if micromegas else r"$C(m_{h^0})$"
            )
        )
        sns.lineplot(
            data=trial_evolution.query(
                f"Model == '{model}' & MicrOMEGAS == {micromegas}  "
            ).replace(nice_sampler_names),
            hue="Sampler",
            x="step",
            y="efficiency",
            legend=True,
            ax=axs[idx, jdx],
        )
        axs[idx, jdx].set_yscale("log")
        axs[idx, jdx].set_xlabel("")
        axs[idx, jdx].set_ylabel("")
        if idx == 1:
            axs[idx, jdx].set_xlabel("Trial Number")
        if jdx == 0:
            axs[idx, jdx].set_ylabel("50 Trials Rolling \n Average Efficiency")

        if idx == 0 and jdx == 1:
            axs[idx, jdx].set_ylim(0.000005, 1.05)
            axs[idx, jdx].legend(loc="lower right")
        else:
            axs[idx, jdx].get_legend().remove()

plt.savefig(
    os.path.join(path_plots, "rolling_average_efficiency_evolution.pdf"), bbox_inches="tight"
)
# %%
for model in ["cMSSM", "pMSSM"]:
    for micromegas in [False, True]:
        f, ax = plt.subplots(figsize=figsize_rect)
        ax.set_title(
            r"{}, {}".format(
                model, r"$C(m_{h^0} \cap \Omega_{DM} h^2)$" if micromegas else r"$C(m_{h^0})$"
            )
        )
        sns.scatterplot(
            data=full_episode_metrics.query(
                f"Model == '{model}' & MicrOMEGAS == {micromegas}"
            ).replace(nice_sampler_names),
            x="mean_distance",
            y="mean_efficiency",
            hue="Sampler",
            ax=ax,
        )
        ax.set_xlabel("Episode Mean Euclidean Distance")
        ax.set_ylabel("Episode Efficiency")
        if micromegas:
            ax.set_yscale("log")
        ax.set_ylim(-0.05, 1.05)
        plt.savefig(
            os.path.join(
                path_plots, f"{model}_mo_{micromegas}_mean_euclidean_distance_vs_efficiency.pdf"
            ),
            bbox_inches="tight",
        )

# %%
for model in ["cMSSM", "pMSSM"]:
    for micromegas in [False, True]:
        f, ax = plt.subplots(figsize=figsize_rect)
        ax.set_title(
            r"{}, {}".format(
                model, r"$C(m_{h^0} \cap \Omega_{DM} h^2)$" if micromegas else r"$C(m_{h^0})$"
            )
        )
        sns.scatterplot(
            data=full_episode_metrics.query(
                f"Model == '{model}' & MicrOMEGAS == {micromegas}"
            ).replace(nice_sampler_names),
            x="wd_distance",
            y="mean_efficiency",
            hue="Sampler",
            ax=ax,
        )
        ax.set_xlabel("Episode Total Wasserstein Distance")
        ax.set_ylabel("Episode Efficiency")
        if micromegas:
            ax.set_yscale("log")
        ax.set_ylim(-0.05, 1.05)
        plt.savefig(
            os.path.join(path_plots, f"{model}_mo_{micromegas}_wd_vs_efficiency.pdf"),
            bbox_inches="tight",
        )


# %%
f, ax = plt.subplots(figsize=figsize_square_grid)
ax.set_title("Average Wasserstein Distance\nfor the cMSSM Parameters in Box Space")
sns.heatmap(
    full_episode_metrics.query(" Model == 'cMSSM' ")
    .groupby(by=["Model", "MicrOMEGAS", "Sampler"])
    .mean()[cMSSM_box_params],
    square=True,
    annot=True,
    linewidths=0.75,
    ax=ax,
    cmap="Blues",
    fmt=".2f",
    xticklabels=[r"$\hat m_0$", r"$\hat m_{1/2}$", r"$\hat A_0$", r"$\hat \tan \beta$"],
    yticklabels=[
        r"$C(m_{h^0})$, Random",
        r"$C(m_{h^0})$, TPE",
        r"$C(m_{h^0})$, NSGA-II",
        r"$C(m_{h^0})$, CMA-ES",
        r"$C(m_{h^0} \cap \Omega_{DM} h^2)$, Random",
        r"$C(m_{h^0} \cap \Omega_{DM} h^2)$, TPE",
        r"$C(m_{h^0} \cap \Omega_{DM} h^2)$, NSGA-II",
        r"$C(m_{h^0} \cap \Omega_{DM} h^2)$, CMA-ES",
    ],
    cbar=False,
)
ax.set_ylabel("")
plt.savefig(os.path.join(path_plots, "cMSSM_heatmap_wds.pdf"), bbox_inches="tight")

# %%
f, ax = plt.subplots(figsize=(20, 10))
ax.set_title("Average Wasserstein Distance\nfor the pMSSM Parameters in Box Space")
sns.heatmap(
    full_episode_metrics.query(" Model == 'pMSSM' ")
    .groupby(by=["Model", "MicrOMEGAS", "Sampler"])
    .mean()[pMSSM_box_params],
    square=True,
    annot=True,
    linewidths=0.75,
    ax=ax,
    cmap="Blues",
    fmt=".2f",
    xticklabels=[
        r"$\hat M_1$",
        r"$\hat M_2$",
        r"$\hat M_3$",
        r"$\hat \mu$",
        r"$\hat A_t$",
        r"$\hat A_b$",
        r"$\hat A_\tau$",
        r"$\hat m_{L_1}$",
        r"$\hat m_{e_1}$",
        r"$\hat m_{L_3}$",
        r"$\hat m_{e_3}$",
        r"$\hat m_{Q_1}$",
        r"$\hat m_{u_1}$",
        r"$\hat m_{d_1}$",
        r"$\hat m_{d_1}$",
        r"$\hat m_{Q_3}$",
        r"$\hat m_{u_3}$",
        r"$\hat m_{d_3}$",
        r"$\hat \tan \beta$",
    ],
    yticklabels=[
        r"$C(m_{h^0})$, Random",
        r"$C(m_{h^0})$, TPE",
        r"$C(m_{h^0})$, NSGA-II",
        r"$C(m_{h^0})$, CMA-ES",
        r"$C(m_{h^0} \cap \Omega_{DM} h^2)$, Random",
        r"$C(m_{h^0} \cap \Omega_{DM} h^2)$, TPE",
        r"$C(m_{h^0} \cap \Omega_{DM} h^2)$, NSGA-II",
        r"$C(m_{h^0} \cap \Omega_{DM} h^2)$, CMA-ES",
    ],
    cbar=False,
)
ax.set_ylabel("")
plt.savefig(os.path.join(path_plots, "pMSSM_heatmap_wds.pdf"), bbox_inches="tight")

# %%
