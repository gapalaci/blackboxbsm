# %%
import os
from functools import reduce

import numpy as np
import pandas as pd
from scipy.spatial.distance import pdist
from scipy.stats import wasserstein_distance
from statsmodels.distributions.empirical_distribution import ECDF

from utils.analysis import *

# %%
# Preamble
run_name = "run_fixed"
path_plots = os.path.join(run_name, "plots")
os.makedirs(path_plots, exist_ok=True)


def get_mean(x):
    """
    Computes mean euclidean distance between points in x.
    """
    return pdist(x).mean()


# %%
# Data
store = pd.HDFStore(os.path.join(run_name, "AllResults.h5"))
full_episode_metrics = pd.DataFrame()
full_trial_evolution = pd.DataFrame()

for key in store.keys():
    print("Processing:", key)
    results = store[key]
    results["MicrOMEGAS"] = results["MicrOMEGAS"] == "True"
    model = results["Model"].iloc[0]
    micromegas = results["MicrOMEGAS"].iloc[0]
    print("Starting with model {} results for MicrOMEGAS={}".format(model, micromegas))

    results["physical"] = (results["value"] < np.inf).astype(int)
    for mass in masses:
        results[f"m_{mass.split('user_attrs_m_')[-1]}"] = results[mass].abs()
    positive_masses = [f"m_{mass.split('user_attrs_m_')[-1]}" for mass in masses]
    samplers = ["random", "tpe", "nsgaii", "cmaes"]
    results["Sampler"] = pd.Categorical(results["Sampler"].tolist(), categories=samplers)

    print("Model: {} | MicrOMEGAS: {} | Computing metrics...".format(model, micromegas))
    print("Model: {} | MicrOMEGAS: {} | Computing episode efficiency.".format(model, micromegas))
    _efficiencies_per_episode = results.groupby(by=["Model", "MicrOMEGAS", "Sampler", "Episode"])[
        "user_attrs_valid_point"
    ].mean()
    _efficiencies_per_episode.name = "mean_efficiency"
    _efficiencies_per_episode = _efficiencies_per_episode.reset_index()
    print("Model: {} | MicrOMEGAS: {} | Done.".format(model, micromegas))

    print(
        "Model: {} | MicrOMEGAS: {} | Computing episode physical mean.".format(model, micromegas)
    )
    _physical_per_episode = results.groupby(by=["Model", "MicrOMEGAS", "Sampler", "Episode"])[
        ["physical"]
    ].mean()
    _physical_per_episode.name = "physical_point_mean"
    _physical_per_episode = _physical_per_episode.reset_index()
    print("Model: {} | MicrOMEGAS: {} | Done.".format(model, micromegas))

    print("Model: {} | MicrOMEGAS: {} | Computing episode trials times.".format(model, micromegas))
    results["datetime_complete"] = pd.to_datetime(results["datetime_complete"])
    results["datetime_start"] = pd.to_datetime(results["datetime_start"])
    results["trial_time"] = (results["datetime_complete"] - results["datetime_start"]).apply(
        lambda x: x.total_seconds()
    )
    _times_per_episode = results.groupby(by=["Model", "MicrOMEGAS", "Sampler", "Episode"])[
        ["trial_time"]
    ].sum()
    _times_per_episode.name = "trial_time_sum"
    _times_per_episode = _times_per_episode.reset_index()
    print("Model: {} | MicrOMEGAS: {} | Done.".format(model, micromegas))

    print(
        "Model: {} | MicrOMEGAS: {} | Computing episode mean distance.".format(model, micromegas)
    )
    _episode_distances = (
        results.query("user_attrs_valid_point == True")
        .groupby(by=["Model", "MicrOMEGAS", "Sampler", "Episode"])[
            [col for col in results.columns if "params_" in col]
        ]
        .apply(get_mean)
    )
    _episode_distances.name = "mean_distance"
    _episode_distances = _episode_distances.reset_index()
    print("Model: {} | MicrOMEGAS: {} | Done.".format(model, micromegas))

    print(
        "Model: {} | MicrOMEGAS: {} | Computing episode Wasserstein Distances.".format(
            model, micromegas
        )
    )
    x1 = np.linspace(0, 1, 2000, endpoint=True)
    u1 = x1
    _wd_per_episode = pd.DataFrame()
    for sampler in results.Sampler.unique():
        print(
            "Model: {} | MicrOMEGAS: {} | Computing Wasserstein Distance for sampler {}".format(
                model, micromegas, sampler
            )
        )
        for episode in results.Episode.unique():
            _wd_episode = pd.DataFrame(dtype="float32")
            _wd_episode.loc[0, "Model"] = model
            _wd_episode.loc[0, "MicrOMEGAS"] = micromegas
            _wd_episode.loc[0, "Sampler"] = sampler
            _wd_episode.loc[0, "Episode"] = episode

            for box in [col for col in results.columns if "params_" in col]:
                try:
                    ecdf_data = ECDF(
                        results.query(
                            "user_attrs_valid_point == True and Sampler == '{}' and Episode == {}".format(
                                sampler, episode
                            )
                        )[box].values
                    )
                    u2 = ecdf_data(u1)
                    wd = wasserstein_distance(u1, u2)
                    _wd_episode.loc[0, box] = wd
                except:
                    _wd_episode.loc[0, box] = 1 / 2
            _wd_per_episode = pd.concat(
                [_wd_per_episode, pd.DataFrame(_wd_episode)], ignore_index=True, sort=True
            )
    _wd_per_episode["wd_distance"] = _wd_per_episode[
        [col for col in results.columns if "params_" in col]
    ].sum(1)
    print("Model: {} | MicrOMEGAS: {} | Done.".format(model, micromegas))

    print("Model: {} | MicrOMEGAS: {} | Joining episode metrics.".format(model, micromegas))
    episode_metrics = reduce(
        lambda left, right: pd.merge(
            left, right, on=["Model", "MicrOMEGAS", "Sampler", "Episode"]
        ),
        [
            _efficiencies_per_episode,
            _physical_per_episode,
            _times_per_episode,
            _episode_distances,
            _wd_per_episode,
        ],
    )
    full_episode_metrics = pd.concat([full_episode_metrics, episode_metrics], ignore_index=True)
    print("Model: {} | MicrOMEGAS: {} | Done.".format(model, micromegas))

    print(
        "Model: {} | MicrOMEGAS: {} | Computing trial rolling statistics.".format(
            model, micromegas
        )
    )
    trial_steps = np.linspace(0, 2000, num=2000 // 50 + 1, endpoint=True, dtype=int)
    value_evolution = pd.DataFrame()
    efficiency_evolution = pd.DataFrame()
    trial_evolution = pd.DataFrame()
    for trial_step in trial_steps:
        rolling_value = (
            results.query(
                f"physical == True and number <= {trial_step} and number > {trial_step}-50 "
            )
            .groupby(["Model", "MicrOMEGAS", "Sampler", "Episode"])[["value"]]
            .mean()
            .reset_index()
        )

        rolling_eff = (
            results.query(
                f"physical == True and number <= {trial_step} and number > {trial_step}-50 "
            )
            .groupby(["Model", "MicrOMEGAS", "Sampler", "Episode"])[["user_attrs_valid_point"]]
            .mean()
            .reset_index()
        )

        rolling_value["step"] = trial_step
        rolling_eff["step"] = trial_step

        value_evolution = pd.concat([value_evolution, rolling_value], ignore_index=True)
        efficiency_evolution = pd.concat([efficiency_evolution, rolling_eff], ignore_index=True)
    efficiency_evolution.rename(columns={"user_attrs_valid_point": "efficiency"}, inplace=True)
    trial_evolution = pd.merge(
        value_evolution,
        efficiency_evolution,
        on=["Model", "MicrOMEGAS", "Sampler", "Episode", "step"],
    )
    full_trial_evolution = pd.concat([full_trial_evolution, trial_evolution], ignore_index=True)
    print("Model: {} | MicrOMEGAS: {} | Done.".format(model, micromegas))


# %%
print("Computing episode metrics statistics.")
mean_efficiency_average = full_episode_metrics.groupby(by=["Model", "MicrOMEGAS", "Sampler"])[
    "mean_efficiency"
].mean()
mean_efficiency_average.name = "mean_efficiency_average"
mean_efficiency_std = full_episode_metrics.groupby(by=["Model", "MicrOMEGAS", "Sampler"])[
    "mean_efficiency"
].std()
mean_efficiency_std.name = "mean_efficiency_std"

physical_average = full_episode_metrics.groupby(by=["Model", "MicrOMEGAS", "Sampler"])[
    "physical"
].mean()
physical_average.name = "physical_average"
physical_std = full_episode_metrics.groupby(by=["Model", "MicrOMEGAS", "Sampler"])[
    "physical"
].std()
physical_std.name = "physical_std"

trial_time_average = full_episode_metrics.groupby(by=["Model", "MicrOMEGAS", "Sampler"])[
    "trial_time"
].mean()
trial_time_average.name = "trial_time_average"
trial_time_std = full_episode_metrics.groupby(by=["Model", "MicrOMEGAS", "Sampler"])[
    "trial_time"
].std()
trial_time_std.name = "trial_time_std"

mean_distance_average = full_episode_metrics.groupby(by=["Model", "MicrOMEGAS", "Sampler"])[
    "mean_distance"
].mean()
mean_distance_average.name = "mean_distance_average"
mean_distance_std = full_episode_metrics.groupby(by=["Model", "MicrOMEGAS", "Sampler"])[
    "mean_distance"
].std()
mean_distance_std.name = "mean_distance_std"

wd_distance_average = full_episode_metrics.groupby(by=["Model", "MicrOMEGAS", "Sampler"])[
    "wd_distance"
].mean()
wd_distance_average.name = "wd_distance_average"
wd_distance_std = full_episode_metrics.groupby(by=["Model", "MicrOMEGAS", "Sampler"])[
    "wd_distance"
].std()
wd_distance_std.name = "wd_distance_std"

# %%
grouped_episode_metrics = reduce(
    lambda x, y: pd.merge(x, y, on=["Model", "MicrOMEGAS", "Sampler"]),
    [
        mean_efficiency_average,
        mean_efficiency_std,
        physical_average,
        physical_std,
        trial_time_average,
        trial_time_std,
        mean_distance_average,
        mean_distance_std,
        wd_distance_average,
        wd_distance_std,
    ],
)
# %%

print("Saving all metrics.")
full_episode_metrics.to_csv(os.path.join(run_name, "full_episode_metrics.csv"))
full_trial_evolution.to_csv(os.path.join(run_name, "trial_evolution.csv"))
grouped_episode_metrics.to_csv(os.path.join(run_name, "grouped_episode_metrics.csv"))
print("All done!")
# %%
