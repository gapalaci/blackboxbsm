cMSSM_params = [
    "user_attrs_MINPAR_m0",
    "user_attrs_MINPAR_m12",
    "user_attrs_MINPAR_A0",
    "user_attrs_MINPAR_cos(phase_mu)",
    "user_attrs_EXTPAR_tan(beta)",
]

cMSSM_box_params = ["params_m0", "params_m12", "params_A0", "params_tanb"]


pMSSM_params = [
    "user_attrs_EXTPAR_A_b",
    "user_attrs_EXTPAR_A_t",
    "user_attrs_EXTPAR_A_tau",
    "user_attrs_EXTPAR_M_1",
    "user_attrs_EXTPAR_M_2",
    "user_attrs_EXTPAR_M_3",
    "user_attrs_EXTPAR_M_D11",
    "user_attrs_EXTPAR_M_D22",
    "user_attrs_EXTPAR_M_D33",
    "user_attrs_EXTPAR_M_E11",
    "user_attrs_EXTPAR_M_E22",
    "user_attrs_EXTPAR_M_E33",
    "user_attrs_EXTPAR_M_L11",
    "user_attrs_EXTPAR_M_L22",
    "user_attrs_EXTPAR_M_L33",
    "user_attrs_EXTPAR_M_Q11",
    "user_attrs_EXTPAR_M_Q22",
    "user_attrs_EXTPAR_M_Q33",
    "user_attrs_EXTPAR_M_U11",
    "user_attrs_EXTPAR_M_U22",
    "user_attrs_EXTPAR_M_U33",
    "user_attrs_EXTPAR_m_A, pole mass",
    "user_attrs_EXTPAR_mu",
    # "user_attrs_EXTPAR_scale_for_input_parameters",
    "user_attrs_EXTPAR_tan(beta)",
]

all_params = cMSSM_params + pMSSM_params

pMSSM_box_params = [
    "params_M1",
    "params_M2",
    "params_M3",
    "params_mu",
    "params_At",
    "params_Ab",
    "params_Atau",
    "params_M_A",
    "params_m_L1",
    "params_m_e1",
    "params_m_L3",
    "params_m_e3",
    "params_m_Q1",
    "params_m_u1",
    "params_m_d1",
    "params_m_Q3",
    "params_m_u3",
    "params_m_d3",
    "params_tan1",
]

nice_cMSSM_params_names = {
    "user_attrs_MINPAR_m0": r"$m_0$ (GeV)",
    "user_attrs_MINPAR_m12": r"$m_{1/2}$ (GeV)",
    "user_attrs_MINPAR_A0": r"$A_0$ (GeV)",
    # "user_attrs_MINPAR_cos(phase_mu)": r"sig$(\mu)$",
    "user_attrs_EXTPAR_tan(beta)": r"$\tan(\beta)$",
}


nice_pMSSM_params_names = {
    "user_attrs_EXTPAR_A_b": r"$A_b$ (GeV)",
    "user_attrs_EXTPAR_A_t": r"$A_t$ (GeV)",
    "user_attrs_EXTPAR_A_tau": r"$A_{\tau}$ (GeV)",
    "user_attrs_EXTPAR_M_1": r"$M_1$ (GeV)",
    "user_attrs_EXTPAR_M_2": r"$M_2$ (GeV)",
    "user_attrs_EXTPAR_M_3": r"$M_3$ (GeV)",
    "user_attrs_EXTPAR_M_D11": r"$m_{d_{1,1}} (GeV)$",
    "user_attrs_EXTPAR_M_D22": r"$m_{d_{2,2}} (GeV)$",
    "user_attrs_EXTPAR_M_D33": r"$m_{d_{3,3}} (GeV)$",
    "user_attrs_EXTPAR_M_E11": r"$m_{e_{1,1}} (GeV)$",
    "user_attrs_EXTPAR_M_E22": r"$m_{e_{2,2}} (GeV)$",
    "user_attrs_EXTPAR_M_E33": r"$m_{e_{3,3}} (GeV)$",
    "user_attrs_EXTPAR_M_L11": r"$m_{L_{1,1}} (GeV)$",
    "user_attrs_EXTPAR_M_L22": r"$m_{L_{2,2}} (GeV)$",
    "user_attrs_EXTPAR_M_L33": r"$m_{L_{3,3}} (GeV)$",
    "user_attrs_EXTPAR_M_Q11": r"$m_{Q_{1,1}} (GeV)$",
    "user_attrs_EXTPAR_M_Q22": r"$m_{Q_{2,2}} (GeV)$",
    "user_attrs_EXTPAR_M_Q33": r"$m_{Q_{3,3}} (GeV)$",
    "user_attrs_EXTPAR_M_U11": r"$m_{u_{1,1}} (GeV)$",
    "user_attrs_EXTPAR_M_U22": r"$m_{u_{2,2}} (GeV)$",
    "user_attrs_EXTPAR_M_U33": r"$m_{u_{3,3}} (GeV)$",
    "user_attrs_EXTPAR_m_A, pole mass": r"$m_A$ (pole) (GeV)",
    "user_attrs_EXTPAR_mu": r"$\mu$ (GeV)",
    # "user_attrs_EXTPAR_scale_for_input_parameters": r"$$",
    "user_attrs_EXTPAR_tan(beta)": r"$\tan(\beta)$",
}

nice_params_names = nice_cMSSM_params_names | nice_pMSSM_params_names

masses = [
    "user_attrs_m_A0",
    "user_attrs_m_H+",
    "user_attrs_m_H0",
    "user_attrs_m_W+",
    "user_attrs_m_Z(pole)",
    "user_attrs_m_h0",
    "user_attrs_m_t(pole)",
    "user_attrs_m_tau(pole)",
    "user_attrs_m_~b_1",
    "user_attrs_m_~b_2",
    "user_attrs_m_~c_L",
    "user_attrs_m_~c_R",
    "user_attrs_m_~chi_1+",
    "user_attrs_m_~chi_10",
    "user_attrs_m_~chi_2+",
    "user_attrs_m_~chi_20",
    "user_attrs_m_~chi_30",
    "user_attrs_m_~chi_40",
    "user_attrs_m_~d_L",
    "user_attrs_m_~d_R",
    "user_attrs_m_~e_L-",
    "user_attrs_m_~e_R-",
    "user_attrs_m_~g",
    "user_attrs_m_~mu_L-",
    "user_attrs_m_~mu_R-",
    "user_attrs_m_~nu_eL",
    "user_attrs_m_~nu_muL",
    "user_attrs_m_~nu_tauL",
    "user_attrs_m_~s_L",
    "user_attrs_m_~s_R",
    "user_attrs_m_~t_1",
    "user_attrs_m_~t_2",
    "user_attrs_m_~tau_1-",
    "user_attrs_m_~tau_2-",
    "user_attrs_m_~u_L",
    "user_attrs_m_~u_R",
]

nice_masses_names = {
    "user_attrs_m_A0": r"$m_{A_0}$",
    "user_attrs_m_H+": r"$m_{H^+}$",
    "user_attrs_m_H0": r"$m_{H^0}$",
    "user_attrs_m_W+": r"$m_{W^+}$",
    "user_attrs_m_Z(pole)": r"$m_{Z}$ (pole)",
    "user_attrs_m_h0": r"$m_{h^0}$",
    "user_attrs_m_t(pole)": r"$m_{t} (pole)$",
    "user_attrs_m_tau(pole)": r"$m_{\tau} (pole)$",
    "user_attrs_m_~b_1": r"$m_{\tilde b_1}$",
    "user_attrs_m_~b_2": r"$m_{\tilde b_2}$",
    "user_attrs_m_~c_L": r"$m_{\tilde c_L}$",
    "user_attrs_m_~c_R": r"$m_{\tilde c_R$",
    "user_attrs_m_~chi_1+": r"$m_{\tilde \chi_1^+}$",
    "user_attrs_m_~chi_10": r"$m_{\tilde \chi_1^0}$",
    "user_attrs_m_~chi_2+": r"$m_{\tilde \chi_2^+}$",
    "user_attrs_m_~chi_20": r"$m_{\tilde \chi_2^0}$",
    "user_attrs_m_~chi_30": r"$m_{\tilde \chi_3^0}$",
    "user_attrs_m_~chi_40": r"$m_{\tilde \chi_4^0}$",
    "user_attrs_m_~d_L": r"$m_{\tilde d_L}$",
    "user_attrs_m_~d_R": r"$m_{\tilde d_R}$",
    "user_attrs_m_~e_L-": r"$m_{\tilde e_L^-}$",
    "user_attrs_m_~e_R-": r"$m_{\tilde e_R^-}$",
    "user_attrs_m_~g": r"$m_{\tild g}$",
    "user_attrs_m_~mu_L-": r"$m_{\tilde \mu_L^-}$",
    "user_attrs_m_~mu_R-": r"$m_{\tilde \mu_R^-}$",
    "user_attrs_m_~nu_eL": r"$m_{\tilde \nu_{e_L}}$",
    "user_attrs_m_~nu_muL": r"$m_{\tilde \nu_{\mu_L}}$",
    "user_attrs_m_~nu_tauL": r"$m_{\tilde \nu_{\tau_L}}$",
    "user_attrs_m_~s_L": r"$m_{\tilde s_L}$",
    "user_attrs_m_~s_R": r"$m_{\tilde s_R}$",
    "user_attrs_m_~t_1": r"$m_{\tilde t_1}$",
    "user_attrs_m_~t_2": r"$m_{\tilde t_2}$",
    "user_attrs_m_~tau_1-": r"$m_{\tilde \tau_1^-}$",
    "user_attrs_m_~tau_2-": r"$m_{\tilde \tau_2^-}$",
    "user_attrs_m_~u_L": r"$m_{\tilde u_L}$",
    "user_attrs_m_~u_R": r"$m_{\tilde u_R}$",
}


M1 = "user_attrs_msoft_M_1"
M2 = "user_attrs_msoft_M_2"
mu = "user_attrs_mhmix_mu"

At = "user_attrs_Au_3,3"
mt = "user_attrs_msoft_M_(Q,33)"

samplers = ["random", "tpe", "nsgaii", "cmaes"]

nice_sampler_names = {"cmaes": "CMA-ES", "nsgaii": "NSGA-II", "tpe": "TPE", "random": "Random"}

figsize_square_grid = (9, 9)
figsize_square_individual = (5, 5)
figsize_rect = (9, 6)
font_scale = 1.3
