#!/usr/bin/env bash
MODEL=$1
SAMPLER=$2
MICROMEGAS=$3
RUNNAME=$4
declare -i CPUCOUNTS
declare -i MAXEPISODES
declare -i OUTCYCLES
CPUCOUNTS=$5
MAXEPISODES=$6
OUTCYCLES=($MAXEPISODES+$CPUCOUNTS-1)/$CPUCOUNTS

WORKING_DIR="$PWD/$RUNNAME/$MODEL/mo-$MICROMEGAS/$SAMPLER"
mkdir -p $WORKING_DIR
ln -sf $PWD/scan.py $WORKING_DIR/scan.py
ln -sf $PWD/defaults.yaml $WORKING_DIR/defaults.yaml
ln -sf $PWD/utils $WORKING_DIR/utils

for j in $(seq 1 $OUTCYCLES); do
    pids=()
    for i in $(seq 1 $CPUCOUNTS); do
        declare -i EPISODE
        EPISODE=$i+$j*$CPUCOUNTS-$CPUCOUNTS
        $PWD/job.sh $EPISODE $MODEL $SAMPLER $MICROMEGAS $RUNNAME &
        pids[$i]=$!
        sleep 1
    done
    for pid in ${pids[*]}; do
        wait $pid
    done
done
exit 0