import numpy as np
import matplotlib.pyplot as plt
O_lb = -5
O_ub = 5

def C(O):
  return max(0, -O+O_lb, O - O_ub)

Cv = np.vectorize(C)
plt.rcParams.update({'font.size': 14})

plt.title(r"Visualisation of $C(\mathcal{O})$")
plt.plot(np.linspace(-10,10, num=100),Cv(np.linspace(-10,10, num=100)))
plt.xlabel(r"$\mathcal{O}$ (dimensions of $\mathcal{O}$)")
plt.ylabel(r"$C(\mathcal{O})$ (dimensions of $\mathcal{O}$)")
plt.xticks([-5,5], [r"$\mathcal{O}_{LB}$", r"$\mathcal{O}_{UB}$"])
plt.yticks([])

plt.savefig("c_plot.pdf")