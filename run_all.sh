#!/usr/bin/env bash
starttime=$(date)
MODELS=('cMSSM' 'pMSSM')
RUNNAME='run_fixed'
CPUCOUNTS=8
MAXEPISODES=500
SAMPLERS=("random" "tpe" "cmaes" "nsgaii")

micromegas='False'
timestamp=$(date)
echo "[${timestamp}] Starting with micromegas ${micromegas}"
for model in "${MODELS[@]}"; do
    timestamp=$(date)
    echo "[${timestamp}] Starting with model ${model}"  
    for sampler in "${SAMPLERS[@]}"; do
        timestamp=$(date)
        echo "[${timestamp}] Starting with sampler ${sampler}"
        ./spawn_jobs.sh $model $sampler $micromegas $RUNNAME $CPUCOUNTS $MAXEPISODES
        pid=$!
    done
    wait $pid
done

micromegas='True'
timestamp=$(date)
echo "[${timestamp}] Starting with micromegas ${micromegas}"
for model in "${MODELS[@]}"; do
    timestamp=$(date)
    echo "[${timestamp}] Starting with model ${model}"
    for sampler in "${SAMPLERS[@]}"; do
        timestamp=$(date)
        echo "[${timestamp}] Starting with sampler ${sampler}"
        ./spawn_jobs.sh $model $sampler $micromegas $RUNNAME $CPUCOUNTS $MAXEPISODES
        pid=$!
    done
    wait $pid
done
timestamp=$(date)
echo "[${timestamp}] All done! (Start time: ${starttime})"
exit 0
